using Dinner.Application.Common.Interfaces.Authentication;
using Dinner.Application.Common.Services;
using Dinner.Infrastructure.Authentication;
using Dinner.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Dinner.Infrastructure;


public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        services.AddSingleton<IDateTimeProvider, DateTimeProvider>();
        services.AddSingleton<IJwtTokenGenerator, JwtTokenGenerator>();
        return services;
    }
}